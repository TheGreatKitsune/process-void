const thing = {
    "nest": {
        "wrapped": function(){
            return thing.nest.value;
        },
        "value": true
    }
}

module.exports = thing;