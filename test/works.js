'use strict';

const processVoid = require('..');
const expect = require('unexpected');

describe('processVoid', () => {
    const _ = new processVoid(null, 'lodash', false);
    const test = new processVoid(null, './test/nestedObject/nestedObject.js', { 'debug': true });

    after(() => {
        _.done();
        test.done();
    })

    it('should successfully initialize', (done) => {
        expect(_, 'to be defined');
        done();
    });

    it('should be able to call functions', function() {
        this.timeout(5000);
        const _ = new processVoid(null, 'lodash', false);
        let result = _.flatten([1, [2]]);
        return expect(result, 'when fulfilled', 'to equal', [1, 2]);
    });

    it('should be able to set deep properties', async function() {
        this.timeout(5000);
        let result = {};
        result.first = await test.nest.wrapped();
        expect(result.first, 'to be true');
        test.nest.value = 'Stuff';
        result.second = await test.nest.wrapped();
        return expect(result.second, 'to equal', 'Stuff');
    })
});